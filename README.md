# Semestral work MVI
## Project: Road Quality Classification

My semestral work is a follow-up work on my thesis. The aim of the 
project is to classify road images into 6 categories by their quality. The dataset 
used for this task if my own and is available on my [GitHub](https://github.com/lenoch0d/road-quality-classification)
with more information and examples. So far the best model is built on MobileNet V1 with 
72% top-1 accuracy and 93% top-2 accuracy on a test set.

Currently, I am facing the following challenges:
- enhancing inference speed which is crucial for end application
- enhancing classification accuracy/per-class f1 scores.

Specifically, my goal is:
- to overview methods for faster inference (pruning, quantization)
- to try different backbones 
  - EfficientNetB{0,1,2}, EfficientNetV2{S, B2, B3} and compare with MobileNet (see results below)
- to further explore data augmentation (rotation, zoom, crop, contrast)

Related papers I have read to: 
- [What is the State of Neural Network Pruning?](https://arxiv.org/pdf/2003.03033.pdf) - meta analysis of pruning methods its studies
- [Neuron Merging: Compensating for Pruned Neurons](https://arxiv.org/pdf/2010.13160.pdf) - novel data-free, one-shot pruning method
- [EfficientNetV2: Smaller Models and Faster Training](https://arxiv.org/pdf/2104.00298.pdf)

## How to:
1. download the dataset from [here](https://github.com/lenoch0d/road-quality-classification)
2. unzip the datasets into folder `data` in the project root and rename the `RQD_test` directory to `export_v3_test` and 
`RGQ_train` to `export_v3_train`


### Running an experiment
Switch to `project_root` directory. With the code below, you can run an experiment with the specified number for the given number of epochs. 
```shell
python experiment.py <your experiment number> <number of epochs> 
``` 

The experiments are defined in `model_definitions.py`. 

Example for running the experiment 226 which is the un-fine-tuned SOTA, defined in `get_exp_226()` function, for 50 epochs:
```shell
python experiment.py 226 50
```

To fine tune it, run the experiment 227:
```shell
python experiment.py 226 30
```

### Experiment evaluation  
After training the finishes, you can evaluate the model:
 
```shell
python evaluate.py 226 -d default_v3_test
```

You can also evaluate on a train set, but it is not much useful:

```shell
python evaluate.py 226 -d default_v3_train
```
