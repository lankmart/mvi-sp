import sys
import os
import time

from utils import *
from model_definitions import get_model

arguments = sys.argv[1:]
if len(arguments) < 2:
    print("Usage: " + sys.argv[0] + " <experiment_number> <epochs> [batch size]")
    sys.exit(1)

experiment_number = int(arguments[0])
epochs = int(arguments[1])
try:
    batch_size = int(arguments[2])
except:
    batch_size = 50

exp_params = list(get_model(experiment_number))

if len(exp_params) == 2:
    exp_params.append(DATASET_DEFAULT)
if len(exp_params) == 3:
    exp_params.append("rgb")
if len(exp_params) == 4:
    exp_params.append(0.25)
if len(exp_params) == 5:
    exp_params.append(False)

model, model_optimizer, dataset, color_space, val_split, use_class_weights = exp_params

train_ds, val_ds, class_names, train_len, val_len, class_weights = load_data(dataset=dataset, batch_size=batch_size,
                                                                             cache_prefetch=False,
                                                                             colorspace=color_space,
                                                                             validation_split=val_split)

model.compile(optimizer=model_optimizer,
              metrics=['accuracy'],
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
              )

model.summary()

checkpoint_folder = "training_" + str(experiment_number)

cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_folder + "/cp.ckpt", save_weights_only=True,
                                                 verbose=1)
stop_callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5)
cp_callback_best = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_folder + "/cp_best.ckpt",
                                                      save_weights_only=True, verbose=1,
                                                      save_best_only=True,
                                                      monitor='val_accuracy' if val_ds is not None else 'accuracy',
                                                      mode='max')

start_time = time.time()
if use_class_weights:
    print(class_weights)

if not os.path.exists(checkpoint_folder):
    os.makedirs(checkpoint_folder)

save_architecture(model, checkpoint_folder, experiment_number)

history = model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=epochs,
    # callbacks=[cp_callback_best, cp_callback]
    callbacks=[cp_callback_best, stop_callback, cp_callback],
    class_weight=class_weights if use_class_weights else None
)

end_time = time.time()

# save_architecture(model, checkpoint_folder, experiment_number)
generate_plots(history, epochs, checkpoint_folder, experiment_number)

print("!!! EXPERIMENT FINISHED !!!")
print("Fitting took " + str(round((end_time - start_time), 2)) + "s")
