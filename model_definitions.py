import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

from utils import DATASET_DEFAULT_V3_TRAIN

img_width = 640
img_height = 480
num_classes = 6

w = 480
h = 360


def get_model(experiment_number):
    try:
        return globals()['get_exp_' + str(experiment_number)]()
    except:
        raise Exception("No model defined for experiment " + str(experiment_number))

def efficent_net(b_ver, dataset, fine_tuning=False, exp_number_to_fine_tune=None):
    if fine_tuning and exp_number_to_fine_tune is None:
        raise Exception("Exp number must be specified for fine tuning!")

    new_dim = 360

    inputs = keras.Input(shape=(img_height, img_width, 3))
    efnets = [
        keras.applications.efficientnet.EfficientNetB0,
        keras.applications.efficientnet.EfficientNetB1,
        keras.applications.efficientnet.EfficientNetB2,
        keras.applications.efficientnet.EfficientNetB3,
        keras.applications.efficientnet.EfficientNetB4,
        keras.applications.efficientnet.EfficientNetB5,
        keras.applications.efficientnet.EfficientNetB6,
        keras.applications.efficientnet.EfficientNetB7,
    ]

    base_model = efnets[b_ver](
        weights='imagenet',  # Load weights pre-trained on ImageNet.
        input_shape=(new_dim, new_dim, 3),
        include_top=False)  # Do not include the ImageNet classifier at the top.

    base_model.trainable = False

    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.Resizing(new_dim, new_dim, interpolation='bilinear'),
    ])

    x = data_augmentation(inputs)
    x = tf.keras.applications.efficientnet.preprocess_input(x)
    x = base_model(x, training=False)
    x = keras.layers.GlobalAveragePooling2D()(x)
    x = keras.layers.Dropout(0.2)(x)

    outputs = keras.layers.Dense(num_classes)(x)
    model = keras.Model(inputs, outputs)

    if fine_tuning:
        model.load_weights('training_' + str(exp_number_to_fine_tune) + '/cp_best.ckpt')
        model.compile(optimizer='adam', metrics=['accuracy'],
                      loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True))
        base_model.trainable = True

        # Freeze all the layers before the `fine_tune_at` layer
        for layer in base_model.layers[:50 + b_ver * 50]:
            layer.trainable = False

    return model, 'adam' if not fine_tuning else keras.optimizers.Adam(1e-5), dataset


def efficent_net_v2(ver, dataset, fine_tuning=False, exp_number_to_fine_tune=None):

    new_dims = {
        '21k-s': 384,
        'b3-360': 360,
        'b3-300': 300
    }

    import tensorflow_hub as hub
    
    version_urls = {
        "21k-s": 'https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet21k_ft1k_s/feature_vector/2',
        "b3-300": 'https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet1k_b3/feature_vector/2',
        "b3-360": 'https://tfhub.dev/google/imagenet/efficientnet_v2_imagenet1k_b3/feature_vector/2'
    }
   
    model = keras.Sequential([
        keras.layers.InputLayer(input_shape=(img_height, img_width, 3)),
        layers.experimental.preprocessing.Resizing(new_dims[ver], new_dims[ver], interpolation='bilinear'),
        layers.experimental.preprocessing.Rescaling(scale=1. / 255),
        hub.KerasLayer(version_urls[ver], trainable=fine_tuning),        
        layers.Dropout(0.2),
        keras.layers.Dense(num_classes, kernel_regularizer=tf.keras.regularizers.l2(0.0001))
    ])


    if exp_number_to_fine_tune is not None:        
        model.load_weights('training_' + str(exp_number_to_fine_tune) + '/cp_best.ckpt')
        model.compile(optimizer='adam', metrics=['accuracy'],
                      loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True))
     
    return model, 'adam' if not fine_tuning else keras.optimizers.Adam(1e-5), dataset


def get_exp_201():
    return xception(DATASET_DEFAULT_V3_TRAIN)


def get_exp_202():
    return xception(DATASET_DEFAULT_V3_TRAIN, True, 201)


def get_exp_203():
    return efficent_net(b_ver=0, dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_204():
    return efficent_net(b_ver=0, dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True, exp_number_to_fine_tune=203)


def get_exp_205():
    return efficent_net(b_ver=1, dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_206():
    return efficent_net(b_ver=1, dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True, exp_number_to_fine_tune=205)


def get_exp_207():
    return efficent_net(b_ver=2, dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_208():
    return efficent_net(b_ver=2, dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True, exp_number_to_fine_tune=207)


def get_exp_209():
    return efficent_net_v2(ver='21k-s', dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_210():
    return efficent_net_v2(ver='21k-s', dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True, exp_number_to_fine_tune=209)


def get_exp_211():
    return efficent_net_v2(ver='b3-300', dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_212():
    return efficent_net_v2(ver='b3-300', dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True,
                           exp_number_to_fine_tune=211)


def get_exp_213():
    return efficent_net_v2(ver='b3-360', dataset=DATASET_DEFAULT_V3_TRAIN)


def get_exp_214():
    return efficent_net_v2(ver='b3-360', dataset=DATASET_DEFAULT_V3_TRAIN, fine_tuning=True,
                           exp_number_to_fine_tune=213)


def mobilenet(dataset, preprocessing: keras.Sequential = None, fine_tuning=False, exp_number=None, new_dim=360):
    if fine_tuning and exp_number is None:
        raise Exception("Exp number must be specified for fine tuning!")

    inputs = keras.Input(shape=(img_height, img_width, 3))

    base_model = keras.applications.MobileNet(
        weights='imagenet',  # Load weights pre-trained on ImageNet.
        input_shape=(new_dim, new_dim, 3),
        include_top=False)  # Do not include the ImageNet classifier at the top.

    base_model.trainable = False

    resize_layer = layers.experimental.preprocessing.Resizing(new_dim, new_dim, interpolation='bilinear')
    if preprocessing is None:
        preprocessing = keras.Sequential([resize_layer])
    # else:
    #     preprocessing.add(resize_layer)

    x = preprocessing(inputs)
    x = tf.keras.applications.mobilenet.preprocess_input(x)
    x = base_model(x, training=False)
    x = keras.layers.GlobalAveragePooling2D()(x)
    x = keras.layers.Dropout(0.2)(x)

    outputs = keras.layers.Dense(num_classes)(x)
    model = keras.Model(inputs, outputs)

    if fine_tuning:
        model.load_weights('training_' + str(exp_number) + '/cp_best.ckpt')
        model.compile(optimizer='adam', metrics=['accuracy'],
                      loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True))
        base_model.trainable = True

    return model, 'adam' if not fine_tuning else keras.optimizers.Adam(1e-5), dataset


def get_exp_215():
    rotation_factor = 0.03  # * 2pi = about 10° degrees rotation
    zoom_factor = (0.234, 0.234)  # when full rotation specified above, the zoom will remove tha paddings

    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomRotation((-rotation_factor, rotation_factor)),
        layers.experimental.preprocessing.RandomZoom(height_factor=zoom_factor, width_factor=zoom_factor),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_216():
    rotation_factor = 0.0138  # * 2pi = about 5° degrees rotation
    zoom_factor = (0.134, 0.134)  # when full rotation specified above, the zoom will remove tha paddings

    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomRotation((-rotation_factor, rotation_factor)),
        layers.experimental.preprocessing.RandomZoom(height_factor=zoom_factor, width_factor=zoom_factor),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_217():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


# 217 fine tuning
def get_exp_218():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 217)


# another 10 epochs of 218 .. best was probably 35 epochs totally BEST SO FAR EVER
def get_exp_219():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 218)


def get_exp_220():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.1),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_221():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.2),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_222():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.3),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_223():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomCrop(360, 360)
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_224():
    dim = 224
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomCrop(dim, dim)
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, new_dim=dim)


# finetuning of 221
def get_exp_225():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.2),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 221)


def get_exp_226():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.2),
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


def get_exp_227():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.2),
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 226)


# fine tuning of 220
def get_exp_228():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.1),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 220)


def get_exp_229():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.1),
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation)


# fine tuning 229
def get_exp_230():
    data_augmentation = keras.Sequential([
        layers.experimental.preprocessing.RandomContrast(factor=0.1),
        layers.experimental.preprocessing.RandomFlip(mode="horizontal"),
        layers.experimental.preprocessing.Resizing(360, 360, interpolation='bilinear')
    ])

    return mobilenet(DATASET_DEFAULT_V3_TRAIN, data_augmentation, True, 229)